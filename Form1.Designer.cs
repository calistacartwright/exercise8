﻿namespace CalorieCalculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fatInputTextBox = new System.Windows.Forms.TextBox();
            this.fatInputLabel = new System.Windows.Forms.Label();
            this.calculateFatButton = new System.Windows.Forms.Button();
            this.fatCaloriesLabel = new System.Windows.Forms.Label();
            this.fatOutputLabel = new System.Windows.Forms.Label();
            this.carbsInputLabel = new System.Windows.Forms.Label();
            this.carbsInputTextBox = new System.Windows.Forms.TextBox();
            this.calculateCarbsButton = new System.Windows.Forms.Button();
            this.carbsCaloriesLabel = new System.Windows.Forms.Label();
            this.carbsOutputLabel = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fatInputTextBox
            // 
            this.fatInputTextBox.Location = new System.Drawing.Point(168, 63);
            this.fatInputTextBox.Name = "fatInputTextBox";
            this.fatInputTextBox.Size = new System.Drawing.Size(100, 20);
            this.fatInputTextBox.TabIndex = 0;
            // 
            // fatInputLabel
            // 
            this.fatInputLabel.AutoSize = true;
            this.fatInputLabel.Location = new System.Drawing.Point(12, 66);
            this.fatInputLabel.Name = "fatInputLabel";
            this.fatInputLabel.Size = new System.Drawing.Size(92, 13);
            this.fatInputLabel.TabIndex = 1;
            this.fatInputLabel.Text = "Input grams of fat:";
            // 
            // calculateFatButton
            // 
            this.calculateFatButton.Location = new System.Drawing.Point(81, 115);
            this.calculateFatButton.Name = "calculateFatButton";
            this.calculateFatButton.Size = new System.Drawing.Size(114, 39);
            this.calculateFatButton.TabIndex = 2;
            this.calculateFatButton.Text = "Calculate Calories from Fat";
            this.calculateFatButton.UseVisualStyleBackColor = true;
            this.calculateFatButton.Click += new System.EventHandler(this.calculateFatButton_Click);
            // 
            // fatCaloriesLabel
            // 
            this.fatCaloriesLabel.AutoSize = true;
            this.fatCaloriesLabel.Location = new System.Drawing.Point(12, 181);
            this.fatCaloriesLabel.Name = "fatCaloriesLabel";
            this.fatCaloriesLabel.Size = new System.Drawing.Size(85, 13);
            this.fatCaloriesLabel.TabIndex = 3;
            this.fatCaloriesLabel.Text = "Calories from fat:";
            // 
            // fatOutputLabel
            // 
            this.fatOutputLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fatOutputLabel.Location = new System.Drawing.Point(168, 180);
            this.fatOutputLabel.Name = "fatOutputLabel";
            this.fatOutputLabel.Size = new System.Drawing.Size(103, 22);
            this.fatOutputLabel.TabIndex = 4;
            // 
            // carbsInputLabel
            // 
            this.carbsInputLabel.AutoSize = true;
            this.carbsInputLabel.Location = new System.Drawing.Point(367, 66);
            this.carbsInputLabel.Name = "carbsInputLabel";
            this.carbsInputLabel.Size = new System.Drawing.Size(106, 13);
            this.carbsInputLabel.TabIndex = 5;
            this.carbsInputLabel.Text = "Input grams of carbs:";
            // 
            // carbsInputTextBox
            // 
            this.carbsInputTextBox.Location = new System.Drawing.Point(546, 63);
            this.carbsInputTextBox.Name = "carbsInputTextBox";
            this.carbsInputTextBox.Size = new System.Drawing.Size(100, 20);
            this.carbsInputTextBox.TabIndex = 6;
            // 
            // calculateCarbsButton
            // 
            this.calculateCarbsButton.Location = new System.Drawing.Point(448, 115);
            this.calculateCarbsButton.Name = "calculateCarbsButton";
            this.calculateCarbsButton.Size = new System.Drawing.Size(111, 39);
            this.calculateCarbsButton.TabIndex = 7;
            this.calculateCarbsButton.Text = "Calculate Calories from Carbs";
            this.calculateCarbsButton.UseVisualStyleBackColor = true;
            this.calculateCarbsButton.Click += new System.EventHandler(this.calculateCarbsButton_Click);
            // 
            // carbsCaloriesLabel
            // 
            this.carbsCaloriesLabel.AutoSize = true;
            this.carbsCaloriesLabel.Location = new System.Drawing.Point(367, 181);
            this.carbsCaloriesLabel.Name = "carbsCaloriesLabel";
            this.carbsCaloriesLabel.Size = new System.Drawing.Size(99, 13);
            this.carbsCaloriesLabel.TabIndex = 8;
            this.carbsCaloriesLabel.Text = "Calories from carbs:";
            // 
            // carbsOutputLabel
            // 
            this.carbsOutputLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.carbsOutputLabel.Location = new System.Drawing.Point(546, 180);
            this.carbsOutputLabel.Name = "carbsOutputLabel";
            this.carbsOutputLabel.Size = new System.Drawing.Size(103, 22);
            this.carbsOutputLabel.TabIndex = 9;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(278, 266);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(103, 32);
            this.clearButton.TabIndex = 10;
            this.clearButton.Text = "Clear All";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 347);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.carbsOutputLabel);
            this.Controls.Add(this.carbsCaloriesLabel);
            this.Controls.Add(this.calculateCarbsButton);
            this.Controls.Add(this.carbsInputTextBox);
            this.Controls.Add(this.carbsInputLabel);
            this.Controls.Add(this.fatOutputLabel);
            this.Controls.Add(this.fatCaloriesLabel);
            this.Controls.Add(this.calculateFatButton);
            this.Controls.Add(this.fatInputLabel);
            this.Controls.Add(this.fatInputTextBox);
            this.Name = "Form1";
            this.Text = "Calorie Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fatInputTextBox;
        private System.Windows.Forms.Label fatInputLabel;
        private System.Windows.Forms.Button calculateFatButton;
        private System.Windows.Forms.Label fatCaloriesLabel;
        private System.Windows.Forms.Label fatOutputLabel;
        private System.Windows.Forms.Label carbsInputLabel;
        private System.Windows.Forms.TextBox carbsInputTextBox;
        private System.Windows.Forms.Button calculateCarbsButton;
        private System.Windows.Forms.Label carbsCaloriesLabel;
        private System.Windows.Forms.Label carbsOutputLabel;
        private System.Windows.Forms.Button clearButton;
    }
}

