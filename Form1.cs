﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalorieCalculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //When called, multiplies any input by 4 to return the amount of calories from carbs.
        public double CarbCalories(double carbs)
        {
            return carbs * 4;
        }

        //When called, multiplies any input by 9 to return the amount of calories from fat.
        public double FatCalories(double fat)
        {
            return fat * 9;
        }

        private void calculateCarbsButton_Click(object sender, EventArgs e)
        {
            double carbGrams;
            double carbCalories;

            //If the text in carbsInputTextBox is a double, call the CarbCalories method.
            if (double.TryParse(carbsInputTextBox.Text, out carbGrams)) 
            {
               carbCalories = CarbCalories(carbGrams);
               
               //The returned value from the CarbCalories method is converted into a string and displayed in a label.
               carbsOutputLabel.Text = carbCalories.ToString();

            }
            //User is prompted to enter a number if input is invalid.
            else
            {
                MessageBox.Show("Please enter a number!");
            }

        }

        private void calculateFatButton_Click(object sender, EventArgs e)
        {
            double fatGrams;
            double fatCalories;

            //if the text in fatInputTextBox is a double, call the FatCalories method.
            if (double.TryParse(fatInputTextBox.Text, out fatGrams))
            {
                fatCalories = FatCalories(fatGrams);

                //The returned value from the FatCalories method is converted into a string and displayed in a label.
                fatOutputLabel.Text = fatCalories.ToString();

            }
            //User is prompted to enter a number if input is invalid.
            else
            {
                MessageBox.Show("Please enter a number!");
            }

        }

        //Clears any output and all user input.
        private void clearButton_Click(object sender, EventArgs e)
        {
            carbsInputTextBox.Text = "";
            carbsOutputLabel.Text = "";
            fatInputTextBox.Text = "";
            fatOutputLabel.Text = "";
        }
    }
}
